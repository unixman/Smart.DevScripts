#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-

import tkinter as tk

window = tk.Tk()

greeting = tk.Label(text="Hello, Tkinter")
greeting.pack()

window.mainloop()

# END
