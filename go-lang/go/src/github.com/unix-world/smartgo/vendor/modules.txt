####### Target: go 1.17 #######
# golang.org/x/sys @ 20230122
golang.org/x/sys/cpu
# golang.org/x/crypto @ 20230122
golang.org/x/crypto/argon2
golang.org/x/crypto/blake2b
golang.org/x/crypto/blowfish
# golang.org/x/text v0.3.5
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# golang.org/x/net @ 20230122
golang.org/x/net/webdav
####### # #######
