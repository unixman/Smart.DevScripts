# github.com/gobwas/httphead v0.1.0
## explicit; go 1.15
github.com/gobwas/httphead
# github.com/gobwas/pool v0.2.1
## explicit
github.com/gobwas/pool
github.com/gobwas/pool/internal/pmath
github.com/gobwas/pool/pbufio
github.com/gobwas/pool/pbytes
# github.com/gobwas/ws v1.2.0
## explicit; go 1.15
github.com/gobwas/ws
github.com/gobwas/ws/wsutil
# github.com/mailru/easygo v0.0.0-20190618140210-3c14a0dc985f
## explicit
github.com/mailru/easygo/netpoll
# golang.org/x/sys v0.6.0
## explicit; go 1.17
golang.org/x/sys/unix
