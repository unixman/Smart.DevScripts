
// GO Lang :: M-DataCenter :: Pg-Backup
// (c) 2020-2022 unix-world.org
// r.20221018.1858 :: STABLE

package main

// DEPENDS executables: 7za, pg_dump

import (
	"log"

	smart 		"github.com/unix-world/smartgo"
	crontab 	"github.com/unix-world/smartgoext/crontab"
	msgpaksrv 	"github.com/unix-world/smartgoext/websocketmsgpak"
)

//--

const (
	CMD_TIMEOUT uint = 3600 * 4 // max 2h per cmd (may need to change this)

	PG_HOST string = "db1.s112.loc"
	PG_PORT string = "5432"
	PG_USER string = "pgsql"
	PG_PASS string = "pgsql"
	PG_DB   string = "s112_live"

	PG_DUMP_FORMAT string 		= "p" // "p" is plain ; "t" is tar
	PG_DUMP_EXECUTABLE string 	= "/opt/msg-server/bin/pg_dump"
	SEVEN_ZIP_EXECUTABLE string = "/opt/msg-server/bin/7za"

	// (all relative paths) for all below the backup dir must be the same ; !!! the backup dir will be deleted and re-created on each new backup action !!!
	BKP_SCHEMA_FILE string = "db-backup/db-schema.sql"
	BKP_DATA_FILE   string = "db-backup/db-data.sql"
	BKP_SAFETY_FILE string = "db-backup/db-pgdump.ok"

	BKP_ARCHIVE_FOLDER string = "webdav/pg-backups"
)

//--

var uxmIsPgBackupTaskRunning bool = false

//--

func initServerInternalTasks(webDavUrl string, intervalMsgSeconds uint32, cliTlsMode string, srvHttpAddr string, srvHttpPort uint16, authUsername string, authPassword string) {
	//--
	log.Println("[DEBUG]", "initServerInternalTasks # @PG-BACKUP@ Task Timing: `" + settings["server-tasks:TaskPgBackupTiming"] + "`")
	//--
	ctab := crontab.New()
	cronJoberr := ctab.AddJob(settings["server-tasks:TaskPgBackupTiming"], func(){ // every hour at minute 45 (for tests)
		if(uxmIsPgBackupTaskRunning != false) {
			log.Println("[NOTICE] ······· ······· SKIP Running the Daily Server Task via Self-Cron Job: PgBackup (the task is already running in background) ·······")
		} else {
			log.Println("[NOTICE] ······· ······· Running the Daily Server Task via Self-Cron Job: PgBackup ·······")
			var archPath string = RunTaskPgBackup()
			var errSetTask string = "Failed: No Archive"
			if(archPath != "") {
				errSetTask = msgpaksrv.MsgPakSetServerTaskCmd("WEBDAV.GET.DB", "[webdav]" + "\n" + "uri = " + webDavUrl + "\n" + "file = " + archPath + "\n" + "cfile = " + archPath + ".size" + "\n", intervalMsgSeconds * 2, cliTlsMode, "", srvHttpAddr, srvHttpPort, authUsername, authPassword)
			} //end if
			if(errSetTask != "") {
				log.Println("[ERROR] `PgBackup` Cron Task Failed # " + errSetTask)
			} //end if
		} //end if
	})
	if(cronJoberr == nil) {
		log.Println("[INFO] Registering the `PgBackup` Cron Task")
	} else {
		log.Println("[ERROR] Failed to Register the `PgBackup` Cron Task")
	} //end if else
} //END FUNCTION

//--

func RunTaskPgBackup() (archPath string) {

	//--
	defer smart.PanicHandler()
	//--

	//--
	uxmIsPgBackupTaskRunning = true
	//--
	defer func() {
		uxmIsPgBackupTaskRunning = false
	}()
	//--

	//--
	var DateTimeStartUtc string = smart.DateNowUtc()
	//--
	log.Println("[INFO] ##### M-DataCenter :: PostgreSQL Backup Task :: " + DateTimeStartUtc + " #####")
	//--

	//--
	checkIfSafePgDumpConnectionParams := func() (areValid bool, errDet string) {
		//--
		if(PG_HOST == "") {
			return false, "Empty PgSQL HOST"
		} //end if
		if((!smart.IsNetValidIpAddr(PG_HOST)) && (!smart.IsNetValidHostName(PG_HOST))) {
			return false, "Invalid PgSQL HOST: " + PG_HOST
		} //end if
		if(PG_PORT == "") {
			return false, "Empty PgSQL PORT"
		} //end if
		if(!smart.IsNetValidPortStr(PG_PORT)) {
			return false, "Invalid PgSQL PORT: " + PG_PORT
		} //end if
		//--
		if(PG_USER == "") {
			return false, "Empty PgSQL USER"
		} //end if
		if(PG_PASS == "") {
			return false, "Empty PgSQL PASS"
		} //end if
		//--
		if(PG_DB == "") {
			return false, "Empty PgSQL DB"
		} //end if
		if((PG_DB == "pgsql") || (PG_DB == "postgres") || (PG_DB == "template0") || (PG_DB == "template1")) {
			return false, "Empty or Invalid or Dissalowed PgSQL DB: " + PG_DB
		} //end if
		//--
		if((PG_DUMP_FORMAT != "p") && (PG_DUMP_FORMAT != "t")) {
			return false, "Empty or Invalid PgSQL Dump Format: " + PG_DUMP_FORMAT
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	checkifSafeBackupDir := func(fDirName string, checkIfExists bool) (isValid bool, errMsg string) {
		//--
		if((smart.StrTrimWhitespaces(fDirName) == "") ||
			smart.PathIsEmptyOrRoot(fDirName) ||
			smart.PathIsBackwardUnsafe(fDirName) ||
			smart.PathIsAbsolute(fDirName) ||
			smart.StrContains(fDirName, ".") ||
			!smart.StrRegexMatchString(smart.REGEX_SMART_SAFE_PATH_NAME, fDirName)) {
			//--
			return false, "Invalid Backup Dir: " + fDirName
			//--
		} //end if
		//--
		if(checkIfExists == true) {
			if(!smart.PathExists(fDirName) || !smart.PathIsDir(fDirName)) {
				return false, "Non-Existing Backup Dir: " + fDirName
			} //end if
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	checkIfSafeBackupFilePath := func(bkpFile string) (isValid bool, errMsg string) {
		//--
		if((smart.StrTrimWhitespaces(bkpFile) == "") ||
			smart.PathIsEmptyOrRoot(bkpFile) ||
			smart.PathIsBackwardUnsafe(bkpFile) ||
			smart.PathIsAbsolute(bkpFile) ||
			smart.StrContains(bkpFile, " ") ||
			!smart.StrRegexMatchString(smart.REGEX_SMART_SAFE_PATH_NAME, bkpFile)) {
			//--
			return false, "Backup File Path (must be relative, non-empty, must not contain spaces, can contain only [ _ a-z A-Z 0-9 - . @ # / ]): " + bkpFile
			//--
		} //end if
		//--
		var fDirName string = smart.PathDirName(bkpFile)
		isDirValid, errDirMsg := checkifSafeBackupDir(fDirName, true)
		if((isDirValid != true) || (errDirMsg != "")) {
			return false, "Invalid Backup Dir: `" + fDirName + "` # " + errDirMsg
		} //end if
		//--
		var fBaseName string = smart.PathBaseName(bkpFile)
		//--
		var theValidFExt = ".sql" // "p", plain
		if(PG_DUMP_FORMAT == "t") {
			theValidFExt = ".tar" // "t", tar
		} //end if else
		//--
		if((smart.StrTrimWhitespaces(fBaseName) == "") ||
			!smart.StrEndsWith(fBaseName, theValidFExt) || (smart.PathBaseExtension(fBaseName) != theValidFExt) ||
			smart.StrStartsWith(fBaseName, ".") ||
			(len(fBaseName) < 5) || (len(smart.StrTrimWhitespaces(fBaseName)) < 5) ||
			!smart.StrRegexMatchString(smart.REGEX_SMART_SAFE_FILE_NAME, fBaseName)) {
			//--
			return false, "Invalid Backup File Name (must ends in .sql, must not start with . and must have min 5 characters): " + fBaseName
			//--
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	cleanupOldFile := func(theFile string) (isValid bool, errMsg string) {
		//--
		if(smart.PathExists(theFile)) {
			if(smart.PathIsDir(theFile)) {
				return false, "The cleanup old file path is a Dir: `" + theFile + "`"
			} //end if
			if(smart.PathIsFile(theFile)) {
				smart.SafePathFileDelete(theFile, false)
			} //end if
			if(smart.PathExists(theFile)) {
				return false, "The cleanup old file path cannot be removed: `" + theFile + "`"
			} //end if
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	getSzFileName := func(bkpFile string) string {
		//--
		var theSzFile string = bkpFile + ".size"
		//--
		return theSzFile
		//--
	} //end function
	//--
	removeSzFile := func(bkpFile string) (isOk bool, errMsg string) {
		//--
		var theSzFile string = getSzFileName(bkpFile)
		isClean, errClean := cleanupOldFile(theSzFile)
		if((isClean != true) || (errClean != "")) {
			return false, "Cannot remove the Old Size File: `" + errClean + "`"
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	createSzFile := func(bkpFile string) (theSize int64, errMsg string) {
		//--
		fSize, errMsg := smart.SafePathFileGetSize(bkpFile, false)
		if(errMsg != "") {
			return 0, "Cannot Get Size of the Backup File (after backup): `" + bkpFile + "` # " + errMsg
		} //end if
		if(fSize <= 0) {
			return 0, "Backup File is empty (after backup) Size: " + smart.ConvertInt64ToStr(fSize) + " bytes # `" + bkpFile + "`"
		} //end if
		//--
		var theSzFile string = getSzFileName(bkpFile)
		//--
		isSuccess, errWrMsg := smart.SafePathFileWrite(theSzFile, "w", false, smart.ConvertInt64ToStr(fSize) + "\n")
		if((isSuccess != true) || (errWrMsg != "")) {
			return fSize, "Failed to save the Size (" + smart.ConvertInt64ToStr(fSize) + ") of File: `" + bkpFile + "` to `" + theSzFile + "` # " + errWrMsg
		} //end if
		//--
		return fSize, ""
		//--
	} //end function
	//--
	getMd5FileName := func(bkpFile string) string {
		//--
		var theMd5ChskumFile string = bkpFile + ".md5"
		//--
		return theMd5ChskumFile
		//--
	} //end function
	//--
	removeMd5Checksum := func(bkpFile string) (isOk bool, errMsg string) {
		//--
		var theMd5ChskumFile string = getMd5FileName(bkpFile)
		isClean, errClean := cleanupOldFile(theMd5ChskumFile)
		if((isClean != true) || (errClean != "")) {
			return false, "Cannot remove the Old MD5 Checksum File: `" + errClean + "`"
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	createMd5Checksum := func(bkpFile string) (md5Hash string, errMsg string) {
		//--
		md5Sum, errMsg := smart.SafePathFileMd5(bkpFile, false)
		if((errMsg != "") || ((md5Sum == "") || (len(md5Sum) != 32))) {
			return "", "Failed to get the MD5 sum of File: `" + bkpFile + "` # " + errMsg
		} //end if
		//--
		var theChecksumFileBaseName string = bkpFile
		if(smart.StrContains(theChecksumFileBaseName, "/")) {
			theChecksumFileBaseName = smart.PathBaseName(theChecksumFileBaseName)
		} //end if
		//--
		var theMd5ChskumFile string = getMd5FileName(bkpFile)
		//--
		isSuccess, errWrMsg := smart.SafePathFileWrite(theMd5ChskumFile, "w", false, "MD5 (" + theChecksumFileBaseName + ") = " + md5Sum + "\n")
		if((isSuccess != true) || (errWrMsg != "")) {
			return "", "Failed to save the MD5 sum (" + md5Sum + ") of File: `" + bkpFile + "` to `" + theMd5ChskumFile + "` # " + errWrMsg
		} //end if
		//--
		return md5Sum, ""
		//--
	} //end function
	//--
	doBackup := func(bkpSchemaOrData string) (isBkpValid bool, errBkpMsg string) {
		//-- defs
		var bkpMode string = ""
		var bkpFile string = ""
		var bkpParam string = ""
		if(bkpSchemaOrData == "schema") {
			bkpMode = "SCHEMA"
			bkpFile = BKP_SCHEMA_FILE
			bkpParam = "--schema-only"
		} else if(bkpSchemaOrData == "data") {
			bkpMode = "DATA"
			bkpFile = BKP_DATA_FILE
			bkpParam = "--data-only"
		} else {
			return false, "Invalid DB Backup Mode: `" + bkpSchemaOrData + "`"
		} //end if else
		//-- check file path to be valid
		isValid, errMsg := checkIfSafeBackupFilePath(bkpFile)
		if((isValid != true) || (errMsg != "")) {
			return false, "Invalid DB Backup File Path: `" + errMsg + "`"
		} //end if
		//-- cleanup old backup
		isClean, errClean := cleanupOldFile(bkpFile)
		if((isClean != true) || (errClean != "")) {
			return false, "Cannot cleanup the old backup: `" + errClean + "`"
		} //end if
		//-- cleanup old md5 cksum
		isMd5Clean, errMd5Clean := removeMd5Checksum(bkpFile)
		if((isMd5Clean != true) || (errMd5Clean != "")) {
			return false, "Cannot cleanup the old backup md5: `" + errMd5Clean + "`"
		} //end if
		//-- cleanup old size reg
		isSzClean, errSzClean := removeSzFile(bkpFile)
		if((isSzClean != true) || (errSzClean != "")) {
			return false, "Cannot cleanup the old backup size registration: `" + errSzClean + "`"
		} //end if
		//-- dump
		var pgDumpDetails string = "Host=" + PG_HOST + ":" + PG_PORT + " ; User=" + PG_USER + " ; Pass=***** ; DB=" + PG_DB + " ; File=" + bkpFile
		log.Println("[NOTICE] ========== PgDump " + bkpMode + ": START ==========")
		log.Println("[DEBUG] PgDump: " + bkpMode + " # " + pgDumpDetails)
		isSuccess, outStd, errStd := smart.ExecTimedCmd(CMD_TIMEOUT, "capture", "capture", "PGPASSWORD=" + PG_PASS, "", PG_DUMP_EXECUTABLE, "--encoding=UTF8", "--blobs", bkpParam, "--no-owner" , "--no-privileges", "--host=" + PG_HOST, "--port=" + PG_PORT, "--user=" + PG_USER, "--format=" + PG_DUMP_FORMAT, "--file=" + bkpFile, PG_DB)
		if(outStd != "") {
			log.Println("[DATA] PgDump Output:", outStd)
		} //end if
		if((isSuccess != true) || (errStd != "")) {
			return false, "PgDump returned Errors / StdErr:\n`" + errStd + "`\n"
		} //end if
		//-- check that backup file exists, after backup
		if((!smart.PathExists(bkpFile)) || (!smart.PathIsFile(bkpFile))) {
			return false, "Backup File cannot be found (after backup) at: `" + bkpFile + "`"
		} //end if
		//-- create size reg file + check file size to be > 0
		fSize, errSize := createSzFile(bkpFile)
		if((fSize <= 0) || (errSize != "")) {
			return false, "Failed to Create the Size Registration File of Backup File (after backup): `" + bkpFile + "`, Size=" + smart.ConvertInt64ToStr(fSize) + " # " + errSize
		} //end if
		//-- create md5 sum file + check sum to be valid
		md5Hash, errMd5Hash := createMd5Checksum(bkpFile)
		if((md5Hash == "") || (len(md5Hash) != 32) || (errMd5Hash != "")) {
			return false, "Failed to Create the MD5 Sum of Backup File (after backup): `" + bkpFile + "` # " + errMd5Hash
		} //end if
		//-- end messages
		log.Println("[OK] DB " + bkpMode + " backup" + " # SIZE(" + smart.ConvertInt64ToStr(fSize) + ") / MD5(" + md5Hash + ") was saved to:", bkpFile)
		log.Println("[NOTICE] ========== PgDump " + bkpMode + ": END ==========")
		//-- @ret
		return true, ""
		//--
	} //end function
	//--
	reCreateDir := func(theDir string) (isOk bool, errMsg string) {
		//--
		if(smart.PathExists(theDir)) {
			if(smart.PathIsFile(theDir)) {
				smart.SafePathFileDelete(theDir, false)
			} else if(smart.PathIsDir(theDir)) {
				smart.SafePathDirDelete(theDir, false)
			} //end if
			if(smart.PathExists(theDir)) {
				return false, "Cannot Cleanup Old Dir: `" + theDir + "`"
			} //end if
		} //end if
		smart.SafePathDirCreate(theDir, true, false) // allow recursive, deny absolute
		if((!smart.PathExists(theDir)) || (!smart.PathIsDir(theDir))) {
			return false, "Cannot Create the New Dir: `" + theDir + "`"
		} //end if
		//--
		return true, ""
		//--
	} //end function
	//--
	logBackupError := func(logMessages ...interface{}) {
		//--
		log.Println("[ERROR] :: BACKUP Task :: ", logMessages) // standard logger
		// must not exit
		//--
	} //end function
	//--

	//--
	if((smart.StrTrimWhitespaces(BKP_ARCHIVE_FOLDER) == "") ||
		(len(BKP_ARCHIVE_FOLDER) < 3) ||
		smart.StrContains(BKP_ARCHIVE_FOLDER, ".") ||
		!smart.StrRegexMatchString(smart.REGEX_SMART_SAFE_PATH_NAME, BKP_ARCHIVE_FOLDER)) {
		//--
		logBackupError("Invalid Backup Archive Dir Name: `" + BKP_ARCHIVE_FOLDER + "`")
		return ""
		//--
	} //end if
	//--
	isArchDirBkOk, errArchDirBk := reCreateDir(BKP_ARCHIVE_FOLDER)
	if((isArchDirBkOk != true) || (errArchDirBk != "")) {
		logBackupError("Backup Archive Folder Cleanup FAILED: # ", errArchDirBk)
		return ""
	} //end if
	//--
	log.Println("[NOTICE] ## Backup Archive Dir was cleared and re-created: `" + BKP_ARCHIVE_FOLDER + "` ##")
	//--

	//--
	dtObjUtc := smart.DateTimeStructUtc("")
	var theArchName = BKP_ARCHIVE_FOLDER + "/" + "pg-db-dump-" + dtObjUtc.Years + dtObjUtc.Months + dtObjUtc.Days + "-" + dtObjUtc.Hours + dtObjUtc.Minutes + dtObjUtc.Seconds + ".7z"
	if((smart.StrTrimWhitespaces(theArchName) == "") ||
		smart.PathIsEmptyOrRoot(theArchName) ||
		smart.PathIsBackwardUnsafe(theArchName) ||
		smart.PathIsAbsolute(theArchName) ||
		smart.StrContains(theArchName, " ") ||
		!smart.StrRegexMatchString(smart.REGEX_SMART_SAFE_PATH_NAME, theArchName)) {
		//--
		logBackupError("Backup Archive File Path in Invalid or Unsafe: `" + theArchName + "`")
		return ""
		//--
	} //end if
	//--

	//--
	var theBkpFolder = smart.PathDirName(BKP_SAFETY_FILE)
	if(smart.StrStartsWith(theBkpFolder, BKP_ARCHIVE_FOLDER)) {
		logBackupError("Backup Archive Dir `" + BKP_ARCHIVE_FOLDER + "` Name must be completely different than the Backup Folder Name `" + "`")
		return ""
	} //end if
	//--
	isValid, errMsg := checkifSafeBackupDir(theBkpFolder, false)
	if((isValid != true) || (errMsg != "")) {
		logBackupError("Invalid Backup Dir: `" + theBkpFolder + "` # " + errMsg)
		return ""
	} //end if
	if((theBkpFolder != smart.PathDirName(BKP_SCHEMA_FILE)) || (theBkpFolder != smart.PathDirName(BKP_DATA_FILE))) {
		logBackupError("Safety File, Schema File and DataFile must be all in the same Dir (currently detected: `" + theBkpFolder + "`)")
		return ""
	} //end if
	//--
	isReDirBkOk, errReDirBk := reCreateDir(theBkpFolder)
	if((isReDirBkOk != true) || (errReDirBk != "")) {
		logBackupError("Backup Folder Cleanup FAILED: # ", errReDirBk)
		return ""
	} //end if
	//--
	log.Println("[NOTICE] ## Backup Dir was cleared and re-created: `" + theBkpFolder + "` ##")
	//--

	//--
	areValid, errDet := checkIfSafePgDumpConnectionParams()
	if((areValid != true) || (errDet != "")) {
		logBackupError("Invalid DB Parameters:", errDet)
		return ""
	} //end if
	//--

	//--
	isClean, errClean := cleanupOldFile(BKP_SAFETY_FILE)
	if((isClean != true) || (errClean != "")) {
		logBackupError("Cannot remove the Old Safety File (`" + BKP_SAFETY_FILE + "`): # " + errClean)
		return ""
	} //end if
	//--

	//-- backup pgsql DB schema
	isBkpSchemaValid, errBkpSchemaMsg := doBackup("schema")
	if((isBkpSchemaValid != true) || (errBkpSchemaMsg != "")) {
		logBackupError("DB Schema Backup Failed:", errBkpSchemaMsg)
		return ""
	} //end if
	//--

	//-- backup pgsql DB data
	isBkpDataValid, errBkpDataMsg := doBackup("data")
	if((isBkpDataValid != true) || (errBkpDataMsg != "")) {
		logBackupError("DB Data Backup Failed:", errBkpDataMsg)
		return ""
	} //end if
	//--

	//--
	var DateTimeEndUtc string = smart.DateNowUtc()
	//--

	//--
	isSuccess, errMsg := smart.SafePathFileWrite(BKP_SAFETY_FILE, "w", false, "START @ " + DateTimeStartUtc + "\n" + "END   @ " + DateTimeEndUtc + "\n")
	if((isSuccess != true) || (errMsg != "")) {
		logBackupError("Failed to create the after-backup Safety File: `" + BKP_SAFETY_FILE + "`", errMsg)
		return ""
	} //end if
	//--

	//--
	log.Println("[NOTICE] ## Backup Archive File is set to: `" + theArchName + "` ##")
	//--
	log.Println("[NOTICE] ========== Archiving (7-Zip): START ==========")
	log.Println("[DEBUG] 7za a: `" + theBkpFolder + "/` > `" + theArchName + "`")
	//--
	isSuccess, outStd, errStd := smart.ExecTimedCmd(CMD_TIMEOUT, "capture", "capture", "", "", SEVEN_ZIP_EXECUTABLE, "a", "-t7z", "-m0=lzma", "-mx=1", "-md=64m", "-ms=off", "-bb0", "-y", theArchName, theBkpFolder + "/")
	if(outStd != "") {
		log.Println("[DATA] 7za Output:", smart.StrReplaceAll(smart.StrReplaceAll(smart.StrTrimWhitespaces(outStd), "\n\n\n", "\n"), "\n\n", "\n"))
	} //end if
	if((isSuccess != true) || (errStd != "")) {
		logBackupError("7-Zip Archiver encountered Errors / StdErr:\n`" + errStd + "`\n")
		return ""
	} //end if
	if((!smart.PathExists(theArchName)) || (!smart.PathIsFile(theArchName))) {
		logBackupError("7-Zip Archive cannot be found (after backup + archiving) `" + theArchName + "`")
		return ""
	} //end if
	fSize, errSize := createSzFile(theArchName)
	if((fSize <= 0) || (errSize != "")) {
		logBackupError("Failed to Create the Size Registration File of Backup Archive (after backup + archiving): `" + theArchName + "`, Size=" + smart.ConvertInt64ToStr(fSize) + " # " + errSize)
		return ""
	} //end if
	//--
	log.Println("[NOTICE] ========== Archiving (7Zip): END ==========")
	//--

	//--
	isReClrDirBkOk, errReClrDirBk := smart.SafePathDirDelete(theBkpFolder, false)
	if((isReClrDirBkOk != true) || (errReClrDirBk != "") || (smart.PathExists(theBkpFolder))) {
		logBackupError("Backup Folder Cleanup (after archiving) FAILED: # ", errReClrDirBk)
		return ""
	} //end if
	//--
	log.Println("[DEBUG] ## Backup Dir was removed (after archiving, to save space): `" + theBkpFolder + "` ##")
	//--

	//--
	log.Println("[OK] ### PgSQL Backup COMPLETED :: " + DateTimeEndUtc + " ###")
	//--
	return theArchName
	//--

} //END FUNCTION

//--

// #END
