
// r.20220418.0316

package main

import (
	"os"
	"log"

	dhkx  "github.com/unix-world/smartgo/dhkx"
	smart "github.com/unix-world/smartgo"
)

const (
	DEBUG bool = false
)

func LogToConsoleWithColors() {
	//--
//	smart.ClearPrintTerminal()
	//--
//	smart.LogToStdErr("DEBUG")
	smart.LogToConsole("DEBUG", true) // colored or not
//	smart.LogToFile("DEBUG", "logs/", "json", true, true) // json | plain ; also on console ; colored or not
	//--
//	log.Println("[DEBUG] Debugging")
//	log.Println("[DATA] Data")
//	log.Println("[NOTICE] Notice")
//	log.Println("[WARNING] Warning")
//	log.Println("[ERROR] Error")
//	log.Println("[OK] OK")
//	log.Println("A log message, with no type")
	//--
} //END FUNCTION


func fatalError(logMessages ...interface{}) {
	//--
	log.Fatal("[ERROR] ", logMessages) // standard logger
	os.Exit(1)
	//--
} //END FUNCTION


func main() {

	LogToConsoleWithColors()

	//--
//	const GroupID int = 14
	var GroupID int = dhkx.DhKxGetRandomGroup(true) // high only
	log.Println("[DATA] GroupID:", GroupID)
	//--
	var keyAPub []byte = nil
	var keyBPub []byte = nil
	var recvCliShard []byte = nil
	//--
	Send := func(side string, data []byte, shard []byte) (e string) {
		if(side == "Server") {
			recvCliShard = shard
			keyBPub = data
		} else if(side == "Client") {
			keyAPub = data
		} else {
			return "Invalid Side: `" + side + "`"
		} //end if else
		log.Println("[DATA] Send to: `", side + "`:", len(data), "bytes")
		return ""
	} //end function
	//--
	Recv := func(side string) (e string, pub []byte, g int, shard []byte) {
		//--
		var pubKey []byte = nil
		//--
		if(side == "Server") {
			pubKey = keyAPub
		} else if(side == "Client") {
			pubKey = keyBPub
		} else {
			return "Invalid Side: `" + side + "`", nil, 0, nil
		} //end if else
		//--
		if(pubKey == nil) {
			return "PubKey Recv from Side: `" + side + "` is NULL", nil, 0, nil
		} //end if
		//--
		log.Println("[DATA] Recv from: `", side + "`:", len(pubKey), "bytes")
		return "", pubKey, GroupID, recvCliShard
		//--
	} //end function
	//--

	//--
	var serverSendToClient dhkx.HandleDhkxSrvSendFunc = func(data []byte, grpID int) string {
		//--
		err := Send("Client", data, nil)
		if(err != "") {
			return "Send (to Client) ERR: " + err
		} //end if
		return ""
		//--
	} //END FUNCTION
	var serverRecvFromClient dhkx.HandleDhkxSrvRecvFunc = func(srvPubKey []byte) (string, []byte, []byte) {
		//--
		err, cliPubKey, _, cliExch := Recv("Client")
		if(err != "") {
			return err, nil, nil
		} //end if
		//--
		return "", cliPubKey, cliExch
		//--
	} //END FUNCTION
	//--
	var clientRecvFromServer dhkx.HandleDhkxCliRecvFunc = func() (string, []byte, int) {
		//--
		err, srvPubKey, grpId, _ := Recv("Server")
		if(err != "") {
			return err, nil, 0
		} //end if
		//--
		return "", srvPubKey, grpId
		//--
	} //END FUNCTION
	var clientSendToServer dhkx.HandleDhkxCliSendFunc = func(data []byte, shard []byte) string {
		//--
		err := Send("Server", data, shard)
		if(err != "") {
			return "Send (to Server) ERR: " + err
		} //end if
		return ""
		//--
	} //END FUNCTION
	//--

	//== DhKx: start
	//-- server: send to client
	errSrvStep1, grpSrv, privSrv, pubSrv := dhkx.DhKxServerInitExchange(GroupID, serverSendToClient)
	if(errSrvStep1 != "") {
		log.Println("[ERROR]: " + errSrvStep1)
		return
	} //end if
	if(DEBUG == true) {
		log.Println("[DEBUG]", "grpSrv:", grpSrv, "privSrv:", privSrv, "pubSrv:", pubSrv)
	} //end if
	//-- client: recv from server + send to server
	errCliRecvSend1, grpCli, privCli, pubCli, recvPubSrv, shardCli, shardExc := dhkx.DhKxClientExchange(clientRecvFromServer, clientSendToServer)
	if(errCliRecvSend1 != "") {
		log.Println("[ERROR]: " + errCliRecvSend1)
		return
	} //end if
	if(DEBUG == true) {
		log.Println("[DEBUG]", "grpCli:", grpCli, "privCli:", privCli, "pubCli:", pubCli, "recvPubSrv:", recvPubSrv, "shardExc:", shardExc)
	} //end if
	//--
	errSrvRecv1GenShardStep2, recvPubCli, shardSrv := dhkx.DhKxServerFinalizeExchange(grpSrv, privSrv, serverRecvFromClient)
	if(errSrvRecv1GenShardStep2 != "") {
		log.Println("[ERROR]: " + errSrvRecv1GenShardStep2)
		return
	} //end if
	if(recvPubCli == nil) {
		log.Println("[ERROR]: SRV.1 CliPubKey is NULL")
		return
	} //end if
	if(smart.StrTrimWhitespaces(smart.Base64Decode(shardSrv)) == "") {
		log.Println("[ERROR]: SRV.2 SharedSecret is NULL")
		return
	} //end if
	//--
	//== DhKx: #end

	log.Println("[INFO] Server's computed Key SHA512:", smart.Sha512(string(shardSrv)))
	log.Println("[INFO] Client's computed Key SHA512:", smart.Sha512(string(shardCli)))
	if(smart.Sha512(string(shardSrv)) != smart.Sha512(string(shardCli))) {
		log.Println("[ERROR] Keys are different")
		return
	} //end if

	log.Println("[INFO] Server's computed Key (B64):", shardSrv)
	log.Println("[INFO] Client's computed Key (B64):", shardCli)
	if(shardSrv != shardCli) {
		log.Println("[ERROR] Keys are different (B64)")
		return
	} //end if

	log.Println("[OK] Keys are similar")

}

// #end
