// GO Lang

package main

import (
	"os"
	"sync/atomic"

	"github.com/tidwall/lotsa"
)


func test1() {

	// Here we load 1,000,000 operations spread over 4 threads.
	var total int64
	lotsa.Ops(1000000, 4,
		func(i, thread int) {
			atomic.AddInt64(&total, 1)
		},
	)
	println(total)

}

func test2() {

	// To output some benchmarking results, set the lotsa.Output prior to calling lotsa.Ops
	var total int64
	lotsa.Output = os.Stdout
	lotsa.Ops(1000000, 4,
		func(i, thread int) {
			atomic.AddInt64(&total, 1)
		},
	)

}

func main() {

	test1()
	test2()

}

// #end
