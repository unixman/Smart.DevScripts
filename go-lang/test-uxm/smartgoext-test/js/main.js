
// jsonInput is comming from golang

let dhKxObj = null;
const testDh = () => {
	if(dhKxObj != null) {
		return dhKxObj;
	} //end if
	let failures = 0;
	let dh = smartJ$DhKx.getData();
	let idz = null;
	if(dh.err) {
		failures++;
	} else {
		idz = smartJ$DhKx.getIdzShadData(dh.idz);
		if(idz.err != '') {
			failures++;
		} else {
			if(idz.shad != dh.srv.shad) {
				failures++;
			} //end if
		} //end if else
	} //end if
	dhKxObj = {
		failures: 	failures,
		dh: 		dh,
		idz: 		idz,
	};
	return dhKxObj;
};

const main = () => {

//	while(true) {} // out of execution time test
//	let array = []; while(true) { array.push(null); } // out of memory test
	console.error('Test: console.error');
	console.warn('Test: console.warn');
	alert('Test: alert');

	console.log('Test BigInt:', BigInt(1000000000000000000000) * BigInt(99999999999999999999999))
	console.log('Test Math:', 999999999999999999999998888n)

	const promise = new Promise(function(resolve, reject) {
		SmartJsVm_sleepTimeMs(10);
		resolve('******* Promise done ... timeout=10ms *******');
	});
	promise.then(
		result => SmartJsVm_consoleLog('[DATA]', result), // shows "done!" after 1 second
		error  => SmartJsVm_consoleLog('[WARNING]', error) // doesn't run
	);

	if(typeof(jsonInput) != 'string') {
		throw Error('ERROR: jsonInput expected from GoLang');
		return '';
	}
	let json;
	try {
		json = JSON.parse(jsonInput)
	} catch(err) {
		throw Error('jsonInput Parse ERROR: ' + err);
		return '';
	}
	if(json['JSON'] !== 'GoLangQuickJsVm') {
		throw Error('jsonInput is INVALID: missing json control field ... json:golang');
		return '';
	} //end if

	const theTxt = String(json['txt']);
	const theHextTxt = String(smartJ$Utils.bin2hex(theTxt));
	const consErr = String(SmartJsVmX_consoleError('This is', 'a', 'log message from a method inside go (this will call a go method)', smartJ$Base64.encode(JSON.stringify(testDh())), String(Date.now())));
	SmartJsVm_sleepTimeMs(1000); // test sleep 1 sec
	const consOK = String(SmartJsVmX_consoleOk(1, 2, 3, String(Date.now())));
	const out = {
		dTS: Date.now(),
		testDh: testDh(),
		b32: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 32),
		b36: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 36),
		b58: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 58),
		b62: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 62),
		b64s: smartJ$BaseEncode.b64s_enc(theTxt),
		b64: smartJ$Base64.encode(theTxt),
		b85: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 85),
		b92: smartJ$BaseEncode.base_from_hex_convert(theHextTxt, 92),
		consErr: consErr,
		consOk : consOK,
		dTE: Date.now(),
	};
	let vErr = JSON.parse(out['consErr']);
	let vOk = JSON.parse(out['consOk']);
	const vDiff = BigInt(vOk['arg:3']) - BigInt(vErr['arg:4']);
	if(vDiff <= 1000) {
		SmartJsVm_consoleLog('[ERROR] The constErr.Arg#4', vErr['arg:4'], 'must be lower than costOk.Arg#3', vOk['arg:3']);
		throw Error('Runtime ERROR: Sync Code Execution Failed !!!');
		return ''
	} else {
		SmartJsVm_consoleLog('[OK]', vErr['arg:4'], '+1000 >=', vOk['arg:3'], 'Diff:', vDiff);
	}

	return String(JSON.stringify(out, null, 2));

};

